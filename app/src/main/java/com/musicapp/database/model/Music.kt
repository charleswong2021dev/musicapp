package com.musicapp.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.musicapp.core.base.BaseResponse

@Entity(tableName = "music_table")
data class Music(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") val id: Int? = null,
    @SerializedName("artistName") val artistName: String? = null,
    @SerializedName("collectionName") val collectionName: String? = null,
    @SerializedName("artworkUrl100") val artworkUrl100: String? = null,

) {

}

