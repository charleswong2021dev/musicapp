package com.musicapp.database

import androidx.room.*
import com.musicapp.database.model.Music


@Database(entities = [Music::class], version = 1)
abstract class MusicDatabase : RoomDatabase() {

    abstract fun musicDao(): MusicDao

//    companion object {
//        @Volatile
//        private var INSTANCE: MusicDatabase? = null
//        fun getDatabase(context: Context): MusicDatabase {
//            val tempInstance = INSTANCE
//            tempInstance?.let {
//                return it
//            }
//
//            synchronized(this) {
//                val instance = Room.databaseBuilder(
//                    context,
//                    MusicDatabase::class.java,
//                    "app_database"
//                ).build()
//                INSTANCE = instance
//                return instance
//            }
//        }
//    }

}