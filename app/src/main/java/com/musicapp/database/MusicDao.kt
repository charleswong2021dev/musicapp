package com.musicapp.database

import androidx.room.*
import com.musicapp.database.model.Music


@Dao
interface MusicDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addMusic(music: Music)

    @Query("DELETE FROM music_table WHERE collectionName = :collectionName and artistName = :artistName and artworkUrl100 = :artworkUrl100")
    fun deleteMusic(collectionName: String, artistName: String, artworkUrl100: String)

    @Query("SELECT * FROM music_table ORDER BY id ASC")
    fun readAllData(): List<Music>
}