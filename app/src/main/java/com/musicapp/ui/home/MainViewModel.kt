package com.musicapp.ui.home

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.musicapp.core.dialog.DialogStyle
import com.musicapp.database.model.Music
import com.musicapp.ui.home.inteface.DbCallBack
import com.musicapp.ui.search.repository.MusicRepo
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MainViewModel(val musicRepo: MusicRepo) : ViewModel() {
    private val _loadingLiveData = MutableLiveData<Boolean>(false)
    val loading: LiveData<Boolean>
        get() = _loadingLiveData

    private val _dialogStyleLiveData = MutableLiveData<DialogStyle>()
    val getDialogStyle: LiveData<DialogStyle>
        get() = _dialogStyleLiveData


    init {

    }


    @SuppressLint("CheckResult")
    fun getBookMarkDataFromDB(callback: DbCallBack) {
        Observable.fromCallable { musicRepo.readAllData() }
            .subscribeOn(Schedulers.io())
            .subscribe({
                it?.let { data ->
                    callback.success(data)
                }
            }, {
                callback.fail()
            }, {
            })
    }

    @SuppressLint("CheckResult")
    fun addBookMark(music: Music) {
        Observable.fromCallable { musicRepo.addMusic(music) }
            .subscribeOn(Schedulers.io())
            .subscribe({

            }, {

            }, {

            })

    }

    @SuppressLint("CheckResult")
    fun deleteBookMark(collectionName: String, artistName: String, artworkUrl100: String) {
        Observable.fromCallable { musicRepo.deleteMusic(collectionName, artistName, artworkUrl100) }
            .subscribeOn(Schedulers.io())
            .subscribe({
            }, {

            }, {

            })

    }

    fun showDialog(dialogStyle: DialogStyle) {
        _dialogStyleLiveData.value = dialogStyle
    }

    fun showLoading(value: Boolean) {
        _loadingLiveData.value = value
    }


}