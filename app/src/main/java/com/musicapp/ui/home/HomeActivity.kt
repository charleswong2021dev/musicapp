package com.musicapp.ui.home

import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.musicapp.core.base.BaseActivity
import com.musicapp.core.dialog.DialogStyle
import com.musicapp.databinding.ActivityHomeBinding
import com.musicapp.ui.bookmark.BookmarkFragment
import com.musicapp.ui.search.SearchFragment

import com.google.android.material.navigation.NavigationBarView
import com.musicapp.R


class HomeActivity : BaseActivity<ActivityHomeBinding>() {

    override fun inflateLayout(layoutInflater: LayoutInflater): ActivityHomeBinding {
        return ActivityHomeBinding.inflate(layoutInflater)
    }

    override fun setUpView() {
        initNavigationView()
    }

    override fun setUpViewModel() {
        loadingViewModel.loading.observe(this, {
            it?.let {
                binding.loadingView.visibility = if (it) View.VISIBLE else View.GONE
            }

        })
        loadingViewModel.getDialogStyle.observe(this, {
            it?.let {
                when (it) {
                    is DialogStyle.DialogNetworkError -> {
                        dialogHelper.errorMessageDialog(it.title, it.message).show()
                    }
                    is DialogStyle.DialogServerError -> {
                        dialogHelper.errorDialog(it.msg).show()
                    }
                }
            }
        })

    }

    private fun initNavigationView() {
        val searchFragment: Fragment = SearchFragment()
        val bookmarkFragment: Fragment = BookmarkFragment()
        val fm: FragmentManager = supportFragmentManager
        var active: Fragment = searchFragment
        fm.beginTransaction().add(R.id.fragmentContainer, bookmarkFragment, "bookmarkFragment")
            .hide(bookmarkFragment).commit()
        fm.beginTransaction().add(R.id.fragmentContainer, searchFragment, "searchFragment").commit()
        binding.bottonNavigationView.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.fragment_search -> {
                    fm.beginTransaction().hide(active).show(searchFragment).commit()
                    active = searchFragment
                    return@OnItemSelectedListener true
                }
                R.id.fragment_bookmark -> {
                    fm.beginTransaction().hide(active).show(bookmarkFragment).commit()
                    active = bookmarkFragment
                    val bookmarkFragment = bookmarkFragment as BookmarkFragment
                    bookmarkFragment.resetBookMark()
                    return@OnItemSelectedListener true
                }

            }
            false
        })
    }

}