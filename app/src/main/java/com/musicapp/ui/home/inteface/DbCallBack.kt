package com.musicapp.ui.home.inteface

import com.musicapp.database.model.Music

interface DbCallBack {
    fun success(bookmarList: List<Music>?)
    fun fail()
}