package com.musicapp.ui.bookmark.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.musicapp.core.base.BaseViewModel
import com.musicapp.database.model.Music
import com.musicapp.ui.home.MainViewModel
import com.musicapp.ui.home.inteface.DbCallBack
import com.musicapp.ui.search.model.GetMusicModel

class BookmarkViewModel(val vm: MainViewModel) : BaseViewModel() {
    private val _bookmarkLiveData = MutableLiveData<List<GetMusicModel.Music>>()
    val bookmark: LiveData<List<GetMusicModel.Music>>
        get() = _bookmarkLiveData

    @SuppressLint("CheckResult")
    fun addBookMark(music: Music) {
        vm.addBookMark(music)
    }

    @SuppressLint("CheckResult")
    fun deleteBookMark(collectionName: String, artistName: String, artworkUrl100: String) {
        vm.deleteBookMark(collectionName, artistName, artworkUrl100)
    }

    fun getBookMarkData() {
        vm.getBookMarkDataFromDB(object : DbCallBack {
            override fun success(bookmarList: List<Music>?) {

                val result = bookmarList?.map {
                    GetMusicModel.Music(
                        artistName = it.artistName,
                        collectionName = it.collectionName,
                        artworkUrl100 = it.artworkUrl100,
                        isBookmark = true
                    )
                }
                result?.let {
                    val reverseList = it.toMutableList().reversed()
                    _bookmarkLiveData.postValue(reverseList)
                }

            }

            override fun fail() {

            }
        })
    }
}