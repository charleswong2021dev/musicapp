package com.musicapp.ui.bookmark

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.musicapp.core.base.BaseFragment
import com.musicapp.database.model.Music
import com.musicapp.databinding.FragmentBookmarkBinding
import com.musicapp.ui.bookmark.viewmodel.BookmarkViewModel
import com.musicapp.ui.event.UpdateSearchListBookmarkEvent
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.greenrobot.eventbus.EventBus




class BookmarkFragment : BaseFragment<FragmentBookmarkBinding>() {
    private val bookmarkViewModel by viewModel<BookmarkViewModel>()
    private lateinit var bookmarkAdapter: BookmarkAdapter

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentBookmarkBinding {
        return FragmentBookmarkBinding.inflate(inflater, container, false)
    }

    override fun setUpView() {
        Log.v("bookmarkFragment","create")
    }

    override fun setUpViewModel() {

        bookmarkViewModel.bookmark.observe(this, {
            it?.let { list ->
                bookmarkAdapter = BookmarkAdapter(list, object : BookmarkAdapter.ItemClickListener {
                    override fun onClickBookmark(isBookmark: Boolean, music: Music) {
                        EventBus.getDefault().post(UpdateSearchListBookmarkEvent(isBookmark, music))
                        if (!isBookmark) {
                            bookmarkViewModel.deleteBookMark(
                                collectionName = music.collectionName.orEmpty(),
                                artistName = music.artistName.orEmpty(),
                                artworkUrl100 = music.artworkUrl100.orEmpty()
                            )
                        } else {
                            bookmarkViewModel.addBookMark(music)
                        }
                    }
                })
                val layoutManager = GridLayoutManager(requireContext(), 2)
                binding?.bookRecyclerView?.apply {
                    setLayoutManager(layoutManager)
                    adapter = bookmarkAdapter
                }
            }

        })
        bookmarkViewModel.getBookMarkData()

    }

    fun resetBookMark() {
        bookmarkViewModel.getBookMarkData()
    }

}