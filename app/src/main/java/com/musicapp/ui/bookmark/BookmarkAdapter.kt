package com.musicapp.ui.bookmark

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.musicapp.R
import com.musicapp.database.model.Music
import com.musicapp.databinding.ItemMusicBinding
import com.musicapp.ui.search.model.GetMusicModel


class BookmarkAdapter(
    private val albumlist: List<GetMusicModel.Music>,
    private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<BookmarkAdapter.BookMarkViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookMarkViewHolder {
        val binding = ItemMusicBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return BookMarkViewHolder(binding, parent.context)
    }

    override fun getItemCount() = albumlist.size

    override fun onBindViewHolder(holder: BookMarkViewHolder, position: Int) {
        albumlist.getOrNull(holder.adapterPosition)?.let {
            with(holder.binding) {
                it.artworkUrl100?.let {
                    Glide.with(holder.context)
                        .load(it)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_collections_24)
                        .into(itemIcon)
                }
                itemCollectionName.text = it.collectionName.orEmpty()
                itemArtistName.text = it.artistName.orEmpty()

                itemBookmark.isChecked = it.isBookmark

                itemBookmark.setOnClickListener { view ->
                    it.isBookmark = itemBookmark.isChecked

                    itemClickListener.onClickBookmark(
                        itemBookmark.isChecked, Music(
                            artistName = it.artistName.orEmpty(),
                            collectionName = it.collectionName.orEmpty(),
                            artworkUrl100 = it.artworkUrl100.orEmpty()
                        )
                    )
                }


            }
        }
    }

    inner class BookMarkViewHolder(val binding: ItemMusicBinding, val context: Context) :
        RecyclerView.ViewHolder(binding.root) {
    }

    interface ItemClickListener {
        fun onClickBookmark(isBookmark: Boolean, music: Music)
    }

}