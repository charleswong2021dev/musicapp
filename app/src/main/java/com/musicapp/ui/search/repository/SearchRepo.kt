package com.musicapp.ui.search

import com.musicapp.api.ApiService
import com.musicapp.api.HandleApiService
import com.musicapp.ui.search.model.GetMusicModel
import io.reactivex.rxjava3.core.Observable

interface SearchRepo {
    fun getMusic(
        term: String,
        entity: String
    ): Observable<GetMusicModel>
}

class SearchRepoImpl(val api: ApiService) : SearchRepo {
    override fun getMusic(
        term: String,
        entity: String
    ): Observable<GetMusicModel> {
        return HandleApiService().getObserbable(api.getMusic(term, entity))
    }
}