package com.musicapp.ui.search.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.musicapp.core.base.BaseViewModel
import com.musicapp.core.dialog.DialogStyle
import com.musicapp.database.model.Music
import com.musicapp.ui.home.MainViewModel
import com.musicapp.ui.home.inteface.DbCallBack
import com.musicapp.ui.search.SearchRepo
import com.musicapp.ui.search.model.GetMusicModel


class SearchViewModel(val vm: MainViewModel, val searchRepo: SearchRepo) :
    BaseViewModel() {

    private val _searchMusicLiveData = MutableLiveData<MutableList<GetMusicModel.Music>>()
    val musicModel: LiveData<MutableList<GetMusicModel.Music>>
        get() = _searchMusicLiveData

    @SuppressLint("CheckResult")
    fun addBookMark(music: Music) {
        vm.addBookMark(music)
    }

    @SuppressLint("CheckResult")
    fun deleteBookMark(collectionName: String, artistName: String, artworkUrl100: String) {
        vm.deleteBookMark(collectionName, artistName, artworkUrl100)
    }

    fun fetchMusic(term: String, entity: String) {
        vm.showLoading(true)
        vm.getBookMarkDataFromDB(object : DbCallBack {

            override fun success(bookmarList: List<Music>?) {
                searchRepo.getMusic(term, entity)
                    .subscribe({ result ->
                        result.results?.map { resultMusic ->
                            if (!bookmarList.isNullOrEmpty()) {
                                bookmarList.map { bookmarkMusic ->
                                    if (resultMusic.artistName == bookmarkMusic.artistName && resultMusic.artworkUrl100 == bookmarkMusic.artworkUrl100
                                        && resultMusic.collectionName == bookmarkMusic.collectionName
                                    ) {
                                        resultMusic.isBookmark = true
                                    }
                                }
                            }
                            resultMusic
                        }?.let {
                            _searchMusicLiveData.value = it.sortedBy { it.artistName }.toMutableList()
                        } ?: kotlin.run {
                        }
                        vm.showLoading(false)
                    }, { error ->
                        vm.showDialog(
                            DialogStyle.DialogNetworkError(
                                "Alert",
                                error.message.orEmpty()
                            )
                        )
                        vm.showLoading(false)
                    }, {

                    })

            }

            override fun fail() {
                vm.showLoading(false)
            }
        })

    }

}