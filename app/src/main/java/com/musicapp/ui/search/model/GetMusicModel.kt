package com.musicapp.ui.search.model

import com.google.gson.annotations.SerializedName
import com.musicapp.core.base.BaseResponse

data class GetMusicModel(
    @SerializedName("resultCount") var resultCount: Int? = null,
    @SerializedName("results") val results: MutableList<Music>? = null
) : BaseResponse() {
    data class Music(
        @SerializedName("wrapperType") val wrapperType: String? = null,
        @SerializedName("kind") val kind: String? = null,
        @SerializedName("artistName") val artistName: String? = null,
        @SerializedName("collectionName") val collectionName: String? = null,
        @SerializedName("trackName") val trackName: String? = null,
        @SerializedName("collectionCensoredName") val collectionCensoredName: String? = null,
        @SerializedName("trackCensoredName") val trackCensoredName: String? = null,
        @SerializedName("artistViewUrl") val artistViewUrl: String? = null,
        @SerializedName("collectionViewUrl") val collectionViewUrl: String? = null,
        @SerializedName("trackViewUrl") val trackViewUrl: String? = null,
        @SerializedName("previewUrl") val previewUrl: String? = null,
        @SerializedName("artworkUrl30") val artworkUrl30: String? = null,
        @SerializedName("artworkUrl60") val artworkUrl60: String? = null,
        @SerializedName("artworkUrl100") val artworkUrl100: String? = null,
        @SerializedName("collectionPrice") val collectionPrice: Float? = null,
        @SerializedName("trackPrice") val trackPrice: Float? = null,
        @SerializedName("releaseDate") val releaseDate: String? = null,
        @SerializedName("collectionExplicitness") val collectionExplicitness: String? = null,
        @SerializedName("trackExplicitness") val trackExplicitness: String? = null,
        @SerializedName("country") val country: String? = null,
        @SerializedName("currency") val currency: String? = null,
        @SerializedName("primaryGenreName") val primaryGenreName: String? = null,
        @SerializedName("isStreamable") val isStreamable: Boolean? = false,
        var isBookmark: Boolean = false

    ) {

    }
}