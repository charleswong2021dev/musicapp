package com.musicapp.ui.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.musicapp.core.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.musicapp.database.model.Music
import com.musicapp.databinding.FragmentSearchBinding
import com.musicapp.ui.event.UpdateSearchListBookmarkEvent
import com.musicapp.ui.search.model.GetMusicModel
import com.musicapp.ui.search.viewmodel.SearchViewModel
import okhttp3.internal.notify
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.ThreadMode

import org.greenrobot.eventbus.Subscribe


class SearchFragment : BaseFragment<FragmentSearchBinding>() {

    private val viewModel by viewModel<SearchViewModel>()
    private lateinit var musicAdapter: MusicAdapter
    private var musicList: MutableList<GetMusicModel.Music> = mutableListOf()

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentSearchBinding {
        return FragmentSearchBinding.inflate(inflater, container, false)
    }

    override fun setUpView() {
    }

    override fun setUpViewModel() {
        viewModel.musicModel.observe(this, { data ->
            data?.let {
                musicList = it
                musicAdapter = MusicAdapter(musicList, object : MusicAdapter.ItemClickListener {
                    override fun onClickBookmark(isBookmark: Boolean, music: Music) {
                        if (!isBookmark) {
                            viewModel.deleteBookMark(
                                collectionName = music.collectionName.orEmpty(),
                                artistName = music.artistName.orEmpty(),
                                artworkUrl100 = music.artworkUrl100.orEmpty()
                            )
                        } else {
                            viewModel.addBookMark(music)
                        }
                    }
                })
                (binding?.searchRecyclerView?.itemAnimator as SimpleItemAnimator).supportsChangeAnimations =
                    false
                val layoutManager = GridLayoutManager(requireContext(), 2)
                binding?.searchRecyclerView?.apply {
                    setLayoutManager(layoutManager)
                    adapter = musicAdapter
                }
            }
        })


        viewModel.fetchMusic("jack johnson", "album")
    }

    fun updateSearchListBookmark(isBookmark: Boolean, music: Music) {
        if (this::musicAdapter.isInitialized && !musicList.isNullOrEmpty()) {
            val data = GetMusicModel.Music(
                artistName = music.artistName, collectionName = music.collectionName,
                artworkUrl100 = music.artworkUrl100
            )
            musicList.let {
                val findIndex = findIndexFromMusicList(data, it)
                if (findIndex != -1) {
                    musicList.getOrNull(findIndex)?.apply {
                        this.isBookmark = isBookmark
                        musicAdapter.notifyItemChanged(findIndex)
                        musicAdapter.notify()
                    }
                }

            }

        }
    }

    private fun findIndexFromMusicList(
        music: GetMusicModel.Music,
        list: MutableList<GetMusicModel.Music>,
        index: Int = 0
    ): Int {
        list.getOrNull(index)?.let {
            if (music.artistName == it.artistName && music.collectionName == it.collectionName && music.artworkUrl100 == it.artworkUrl100) {
                return index
            }
        } ?: kotlin.run {
            return -1
        }
        val nextIndex = index + 1
        return findIndexFromMusicList(music, list, nextIndex)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun UpdateSearchListBookmarkEvent(event: UpdateSearchListBookmarkEvent) {
        Log.v("searchFragment", "UpdateSearchListBookmarkEvent")
        updateSearchListBookmark(event.isBookmark, event.music)


    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

}