package com.musicapp.ui.search

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.musicapp.R
import com.musicapp.database.model.Music

import com.musicapp.databinding.ItemMusicBinding
import com.musicapp.ui.search.model.GetMusicModel


class MusicAdapter(
    private val albumlist: List<GetMusicModel.Music>,
    private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<MusicAdapter.MusicViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicViewHolder {
        val binding = ItemMusicBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return MusicViewHolder(binding, parent.context)
    }

    override fun getItemCount() = albumlist.size

    override fun onBindViewHolder(holder: MusicViewHolder, position: Int) {
        albumlist.getOrNull(holder.adapterPosition)?.let {
            with(holder.binding) {
                it.artworkUrl100?.let {
                    Glide.with(holder.context)
                        .load(it)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_collections_24)
                        .into(itemIcon)
                }
                itemCollectionName.text = it.collectionName.orEmpty()
                itemArtistName.text = it.artistName.orEmpty()

                itemBookmark.isChecked = it.isBookmark

                itemBookmark.setOnClickListener { view ->
                    it.isBookmark = itemBookmark.isChecked

                    itemClickListener.onClickBookmark(
                        itemBookmark.isChecked, Music(
                            artistName = it.artistName.orEmpty(),
                            collectionName = it.collectionName.orEmpty(),
                            artworkUrl100 = it.artworkUrl100.orEmpty()
                        )
                    )
                }


            }
        }
    }

    inner class MusicViewHolder(val binding: ItemMusicBinding, val context: Context) :
        RecyclerView.ViewHolder(binding.root) {
    }

    interface ItemClickListener {
        fun onClickBookmark(isBookmark: Boolean, music: Music)
    }

}