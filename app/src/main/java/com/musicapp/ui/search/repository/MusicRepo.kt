package com.musicapp.ui.search.repository

import com.musicapp.database.model.Music
import com.musicapp.database.MusicDao

interface MusicRepo {
    fun readAllData(
    ): List<Music>

    fun addMusic(music: Music)
    fun deleteMusic(collectionName: String, artistName: String, artworkUrl100: String)
}

class MusicRepoImpl(private val musicDao: MusicDao) : MusicRepo {

    override fun readAllData(): List<Music> {
        return musicDao.readAllData()
    }

    override fun addMusic(music: Music) {
        musicDao.addMusic(music)
    }

    override fun deleteMusic(collectionName: String, artistName: String, artworkUrl100: String) {
        musicDao.deleteMusic(collectionName, artistName, artworkUrl100)
    }
}