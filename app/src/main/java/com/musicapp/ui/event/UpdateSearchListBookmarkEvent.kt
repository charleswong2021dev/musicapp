package com.musicapp.ui.event

import com.musicapp.database.model.Music

class UpdateSearchListBookmarkEvent(val isBookmark: Boolean,val music: Music) {
}