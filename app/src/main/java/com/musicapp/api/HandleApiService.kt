package com.musicapp.api

import androidx.annotation.NonNull
import retrofit2.HttpException
import io.reactivex.rxjava3.core.*
import retrofit2.Call
import retrofit2.Callback
import java.lang.Exception



class HandleApiService {

    fun <T> getObserbable(@NonNull reponse: Call<T>): Observable<T> {
        return Observable.create(object : ObservableOnSubscribe<T> {
            @Throws(Exception::class)
            override fun subscribe(emitter: ObservableEmitter<T>) {
                reponse.enqueue(object : Callback<T> {
                    override fun onResponse(call: Call<T>?, response: retrofit2.Response<T>) {
                        if (!emitter.isDisposed) {
                            response.body()?.let {
                                emitter.onNext(it)
                            }
                        }
                    }

                    override fun onFailure(call: Call<T>?, throwable: Throwable?) {
                        if (!emitter.isDisposed) {
                            if (throwable is HttpException) {
                                val exception = throwable as HttpException
                                when (exception.code()) {
                                    in 400..404 -> {
                                        emitter.onError(Throwable(message = "no network connection"))
                                    }
                                    500 -> {
                                        emitter.onError(throwable)
                                    }
                                    else -> {
                                        emitter.onError(Throwable(message = "no network connection"))
                                    }
                                }
                            } else {
                                emitter.onError(Throwable(message = "no network connection"))
                            }

                        }
                    }
                })
            }
        })
    }

}





