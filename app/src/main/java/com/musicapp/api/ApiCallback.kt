package com.musicapp.api

interface ApiCallback<in T> {
    fun onSuccess(response: T?)
    fun onFailure(errorMsg: String, errorCode: String)
}