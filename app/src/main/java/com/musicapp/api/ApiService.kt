package com.musicapp.api

import com.musicapp.ui.search.model.GetMusicModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/search")
    fun getMusic(@Query("term") term: String, @Query("entity") entity: String): Call<GetMusicModel>
}