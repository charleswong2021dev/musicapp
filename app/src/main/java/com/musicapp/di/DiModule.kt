package com.musicapp.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.musicapp.ui.home.MainViewModel
import com.musicapp.api.ApiService
import com.musicapp.api.RetrofitClient
import com.musicapp.database.MusicDao
import com.musicapp.database.MusicDatabase
import com.musicapp.ui.bookmark.viewmodel.BookmarkViewModel
import com.musicapp.ui.search.SearchRepoImpl


import com.musicapp.ui.search.SearchRepo
import com.musicapp.ui.search.repository.MusicRepo
import com.musicapp.ui.search.repository.MusicRepoImpl
import com.musicapp.ui.search.viewmodel.SearchViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.koinApplication
import org.koin.dsl.module

class DiModule(val context: Context) {
    private val networkModule = module {
        single<ApiService> { RetrofitClient.getApiService }
        single {
            Room.databaseBuilder(
                context,
                MusicDatabase::class.java,
                "app_database"
            ).build()
        }
        single<MusicDao> { get<MusicDatabase>().musicDao() }
    }

    private val repositoryModule = module {
        factory<SearchRepo> { SearchRepoImpl(api = get()) }
        factory<MusicRepo> { MusicRepoImpl(musicDao = get()) }
    }

    private val viewModelModule = module {
        single { MainViewModel(musicRepo = get()) }
        viewModel { SearchViewModel(vm = get(), searchRepo = get()) }
        viewModel { BookmarkViewModel(vm = get()) }
    }

    fun init() {
        startKoin {
            androidContext(context)
            modules(
                listOf(
                    viewModelModule,
                    repositoryModule,
                    networkModule
                )
            )
        }
    }
}


