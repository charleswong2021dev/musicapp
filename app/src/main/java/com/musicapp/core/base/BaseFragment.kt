package com.musicapp.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.musicapp.ui.home.HomeActivity

abstract class BaseFragment<VB : ViewBinding>() : Fragment() {
    protected var mainActivity: HomeActivity? = null
    var binding: VB? = null

    open fun setUpView() {}
    open fun setUpViewModel() {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = inflateLayout(inflater, container, savedInstanceState)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = requireActivity() as HomeActivity
        setUpView()
        setUpViewModel()
    }

    abstract fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): VB


    override fun onDestroy() {
        super.onDestroy()
        binding = null

    }
}