package com.musicapp.core.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.musicapp.core.dialog.DialogHelper
import com.musicapp.ui.home.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {
    protected lateinit var binding: VB
    val loadingViewModel by viewModel<MainViewModel>()
    val dialogHelper by lazy { DialogHelper(context = this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = inflateLayout(layoutInflater)
        setContentView(binding.root)
        setUpView()
        setUpViewModel()
    }


    open fun setUpView() {}
    open fun setUpViewModel() {}

    abstract fun inflateLayout(layoutInflater: LayoutInflater): VB

}