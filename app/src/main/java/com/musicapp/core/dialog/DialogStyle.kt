package com.musicapp.core.dialog

sealed class DialogStyle {
    class DialogNetworkError(val title: String, val message: String) : DialogStyle()
    class DialogServerError(val msg: String) : DialogStyle()
}