package com.musicapp.core.dialog

import android.R
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface


class DialogHelper(val context: Context) {

    fun errorMessageDialog(title: String, msg: String): AlertDialog.Builder {
        return AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton(
                R.string.ok,
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })
            .setIcon(R.drawable.ic_dialog_alert)
    }

    fun errorDialog(msg: String): AlertDialog.Builder {
        return AlertDialog.Builder(context)
            .setMessage(msg)
            .setPositiveButton(
                R.string.ok,
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })
            .setIcon(R.drawable.ic_dialog_alert)
    }


}