package com.musicapp

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.musicapp.di.DiModule

class MyApp : Application() {
    companion object {
        private var instance: MyApp? = null

        fun getInstance(): MyApp? {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        if (instance == null) {
            instance = this
        }
        DiModule(instance!!).init()
    }

    fun hasNetwork(): Boolean {
        val cm: ConnectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val aciveNetwork = cm.activeNetworkInfo
        return aciveNetwork != null && aciveNetwork.isConnectedOrConnecting
    }
}